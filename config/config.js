const mongoose = require('mongoose');
const chalk = require('chalk');
const redis = require('redis');

const connected = chalk.bold.cyan;
const error = chalk.bold.yellow;
const disconnected = chalk.bold.red;
const termination = chalk.bold.magenta;

const dbURL = 'mongodb://localhost:27017/users';
mongoose.connect(dbURL);


//MONGO
const conn = mongoose.connection;

conn.on('error', function(err){
    console.log(error("Mongoose default connection has occured "+err+" error"));
});

conn.once('connected', function(){
  console.log('database connected');
});

conn.on('disconnected', function(){
    console.log(disconnected("Mongoose default connection is disconnected"));
});

process.on('SIGINT', function(){
    conn.close(function(){
        console.log(termination("Mongoose default connection is disconnected due to application termination"));
        process.exit(0)
    });
});




module.exports = {
    mongoose: mongoose,
    connection: conn
}
