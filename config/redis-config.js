const redis = require('redis');

let client = redis.createClient();

client.on('error', function (error) {
    console.log('Redis error: ' + error);
});


client.on('connect', function () {
    console.log('Redis connected');
});

module.exports = client;