
function changeClass(name)
{
  let menuElem = document.getElementById(name)
    let titleElem = menuElem.querySelector('.title')
      menuElem.classList.toggle('open')

};







$(async function(){
  let events;

  $.ajax({
    url: '/event/get-events',
    success: function(data){
      events = data;
    }
  });
  $('#calendar').fullCalendar({
    
    dayClick: function(date, event, view){
      let clickDate = date.format();
      if (!$("#start").val())
      {
        $("#start").val(clickDate);
      }else{
      $("#end").val(clickDate);
    }
      $("#dialog").dialog('open')
    },
    eventSources: [
      {
        events: events,
      }
    ]
  });
});


