const mongoose = require('./../config/config').mongoose;
const Schema = mongoose.Schema;
const bcrypt = require('bcryptjs');
const ObjectId = Schema.ObjectId;

const user_dataSchema = new Schema({
  login : {type : String},
  password : {type : String},
  role : {type : Number},
  active : {type : Boolean},
  comments : [{type: ObjectId, ref:'Comments'}],
} , {
    versionKey: false,
    collection: "UserData"
});

const User = mongoose.model('UserData', user_dataSchema);

module.exports = User;
