const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const Cookies = require('cookies');
const jwt = require('jsonwebtoken');
const {Like} = require('../models/like_model');


const categorie_dataSchema = new Schema({
  name : {type : String},
  likes : [{type: ObjectId, ref:'Like'}],
  dish: [{type: ObjectId, ref:'Dish'}],
  
});
const removeCategorie= async (doc)=> {
      doc.dish.forEach(async(element) => {
      await Dish.findOneAndRemove({'_id': element});
      await Like.findOneAndRemove({'dishId': element});
  });
  doc.likes.forEach(async(element) => {
    await Like.findOneAndRemove({'catId': element})
  })
};
categorie_dataSchema.post('findOneAndDelete', removeCategorie);

const dish_dataSchema = new Schema({
  categorie: {type: ObjectId, ref:'Categorie'},
  nameDish: {type : String, unique: true},
  description: {type: String},
  price: {type: String},
  likes : [{type: ObjectId, ref:'Like'}],
  comments : [{type: ObjectId, ref:'CommentsData'}],
});

const removeDish = async (doc)=> {
  let categorie = await Categorie.findById(doc.categorie);
  let event = await Event.find();
   categorie.dish.forEach(async (elem, i)=> {
    if (elem!==doc._id){
    
      await categorie.dish.splice(i, 1);
      categorie.save();
    }
  })
  doc.likes.forEach(async (element) => {
    await Like.findOneAndRemove({'_id': element})
  })
  
};

dish_dataSchema.post('findOneAndDelete', removeDish)

const event_dataSchema = new Schema({
          user: {type: ObjectId, ref:'User'},
          title: [{type: ObjectId, ref:'Dish'}],
          start: {type: Date},
          end: {type: Date},
    
});

categorie_dataSchema.post('findOneAndDelete', removeCategorie);
const Categorie = mongoose.model('categorie', categorie_dataSchema);
const Dish = mongoose.model('dish', dish_dataSchema);
const Event = mongoose.model('event', event_dataSchema);
module.exports = {Categorie, Dish, Event}
