const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const {Categorie} = require('../models/categorie_model');
const {Dish} = require('../models/categorie_model');

const like_dataSchema = new Schema({
    userId : {type: ObjectId, ref: 'User'},
    catId : {type : ObjectId, ref: 'Categorie'},
    dishId : {type: ObjectId, ref: 'Dish'},
  });



const Like = mongoose.model('like', like_dataSchema);

module.exports={Like}
