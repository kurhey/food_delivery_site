const mongoose = require('./../config/config').mongoose;
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;


const comments_dataSchema = new Schema(
    {     
        user_id : { type : ObjectId, ref: "UserData"},  
        dish_id : { type : ObjectId, ref: "dishes"},
        category_id : { type : ObjectId, ref: "categorie"},
        text : { type : String},
        time : {type : Date}
    }, {
        collection : "CommentsData"
    }
);



module.exports = {
    CommentsData: mongoose.model('CommentsData', comments_dataSchema)
}

