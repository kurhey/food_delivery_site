const Secrets={
    accesSecret: "secret-access-token",
    refreshSecret: "secret-refresh-token",
    accessLife: 5000,
    refreshLife: 2000000
}
module.exports = Secrets;