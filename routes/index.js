"use strict"
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const Cookies = require('cookies');


const registrController = require('./../controllers/registr-controller');
const validadionController = require('./../controllers/valid-controller');
const authController = require('./../controllers/auth-controller')
const bodyParser = require('body-parser');
const resetController = require('./../controllers/resetPass-controller');
const verificationController = require('./../controllers/token_verification-controller');

router.all('/mainmenu/*', verificationController.tokenVerification);


/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('index', { title: 'Log In', info: "" });
});



router.get('/registr', (req, res) => {
  res.render('register', { title: 'Registration', info: ""  });
});
//router.post('/registr', registrController.registr);


router.get('/validation', validadionController.valid);


router.post('/auth', authController.auth);

// RESET PASSWORD
router.get('/reset_pass', (req, res) => {
  res.render('reset_pass', {title: "Reset password", info: ''});
})

router.post('/reset_pass', resetController.passReset);



module.exports = router;
