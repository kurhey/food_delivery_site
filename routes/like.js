var express = require('express');
var router = express.Router();
const Cookies = require('cookies');
const jwt = require('jsonwebtoken');
const {Dish} = require('../models/categorie_model');
const {Categorie} = require('../models/categorie_model');
const {Like} = require('../models/like_model');
const url = require('url');


router.post('/insert-categorieLike', async (req, res, next) =>{
    let url_parts = url.parse(req.url, true);
    let query = url_parts.query;
    let name = req.query.categorie;
    let user = req.body.user;
    const likeCateg = await Categorie.findOne({name: name});
    console.log(user)
    const like = new Like({
        userId : user.id ,
        catId : likeCateg._id ,
    });
    
                  like.save()
                  .then(result => {   
                  likeCateg.likes.push(like)
                  likeCateg.save()
                             .then(result=>{ res.render('addPage', {user: user});
                            })
     
            })
            .catch(err => {
             console.log(err);
             res.status(500).json({
                 error: err
             })
         });   
        })


        router.post('/insert-dishLike', async (req, res, next) =>{
            let url_parts = url.parse(req.url, true);
            let query = url_parts.query;
            let dish_name = req.query.dish;
            let categorie_name = req.query.categorie;

            console.log(categorie_name)
            let likeDish = await Dish.findOne({nameDish: dish_name});

            // let dishes = await Dish.find({categorie : })
            // let categories = await Categorie.find({categorie : })

            let cookies = await new Cookies(req, res);
            let token = cookies.get("refreshToken");
            let user = await jwt.decode(token);
            let excLike = await Like.findOne({dishId: likeDish._id, userId: user.id});

            console.log(excLike)
            const like = new Like({
                userId : user.id ,
                dishId : likeDish._id ,
            });
            if(excLike==null){
                          like.save()
                          .then(result => {   
                          likeDish.likes.push(like)
                          likeDish.save()
                                     .then(result=>{ res.redirect('/mainmenu/dishes?categorie='+ categorie_name);
                                    })
             
                    })
                    .catch(err => {
                     console.log(err);
                     res.status(500).json({
                         error: err
                     })
                 });   
                }else{
                    
                   await Like.findOneAndDelete({dishId: likeDish._id});
                   
                    res.redirect('/mainmenu/dishes?categorie='+ categorie_name)
                }
                })
        
module.exports = router;