var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
const Cookies = require('cookies');
const jwt = require('jsonwebtoken');
const url = require('url');
const multiparty = require('multiparty');
const {
  Categorie
} = require('../models/categorie_model');
const {
  Dish
} = require('../models/categorie_model');
const Comments = require('./../models/comments_model').CommentsData;
const User = require('./../models/user_model');


const dataController = require('./../controllers/data-controller');

router.get('/', dataController.getData);


router.get('/onMain', async (req, res, next) => {
  res.redirect('/mainmenu');
})


router.get('/calendar', async (req, res, next) => {
  let user = req.body.user;
  res.render('calendar', {
    user: user
  });
});


router.post('/logout', (req, res) => {
  let cookies = new Cookies(req, res);
  res.clearCookie('accessToken');
  res.clearCookie('refreshToken');
  res.redirect('/');
});

router.post('/insert-categorie', async (req, res, next) => {
  let user = req.body.user;

  let categorie = new Categorie({
    name: req.body.name,
  });
  let excCateg = await Categorie.find({
    name: req.body.name
  });
  if (excCateg.length != 0) {

    res.render('admin_addPage', {
      username: user.login,
      message: 'Категория с таким названием уже существует'

    });
  } else {
    categorie.save()
      .then(result => {
        res.render('admin_addPage', {
          username: user.login,
          message: "Добавлено"
        });

      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        })
      });
  }
});



router.post('/insert-dish', async(req, res, next) => {
  let cookies = new Cookies(req, res);
  let accessToken = cookies.get('accessToken');
  let user = await jwt.decode(accessToken);
  let categorie = await Categorie.findOne({name: req.body.name});
  let dish = new Dish({
    categorie: categorie._id,
    nameDish: req.body.dishName,
    description: req.body.description,
    price: req.body.price,
  });
  dish
    .save()
    .then(dish => {
      categorie.dish.push(dish);
      categorie.save();

      res.render('admin_addPage', {
        username: user.login,
        message: 'Блюдо добавлено'
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      })
    });

});
router.post('/delete-categorie', async (req, res, next) => {
  let cookies = new Cookies(req, res);
  let accessToken = cookies.get('accessToken');
  let user = await jwt.decode(accessToken);
  Categorie.findOneAndDelete({
    name: req.body.name
  }, function (err) {
    if (err) {
      console.log(err);
      return res.status(500).send();
    }
    return res.render('admin_addPage', {username: user, message: 'Удалено'});
  });

});
router.post('/delete-dish', async (req, res, next) => {
  let cookies = new Cookies(req, res);
  let accessToken = cookies.get('accessToken');
  let user = await jwt.decode(accessToken);
  Dish.findOneAndDelete({
    nameDish: req.body.name
  }, function (err) {
    if (err) {
      console.log(err);
      return res.status(500).send();
    }
    return res.render('admin_addPage', {username: user, message: 'Удалено'});
  })
});


router.post('/change-categorie', async (req, res, next) =>{
  let cookies = new Cookies(req, res);
  let accessToken = cookies.get('accessToken');
  let user = await jwt.decode(accessToken);
  let categorie = await Categorie.findOne({name: req.body.name})
  categorie.update({name: req.body.newName})
            .then(() => {
         res.render('admin_addPage', {username: user, message: 'Изменено'});
  })
            .catch(err => {
                 console.log(err);
                  res.status(500).json({
                  error: err
                    })
                      });
  })
  router.post('/change-dish', async (req, res, next) =>{
  let cookies = new Cookies(req, res);
  let accessToken = cookies.get('accessToken');
  let user = await jwt.decode(accessToken);
  let dish = await Dish.findOne({nameDish: req.body.name});

  dish.update({nameDish: req.body.newName})
            .then(() => {
          res.render('admin_addPage', {username: user, message: 'Изменено'});
  })
            .catch(err => {
                  console.log(err);
                  res.status(500).json({
                  error: err
                    })
                      });
  })

router.get('/most-liked', async (req, res, next) =>{
  let cookies = new Cookies(req, res);
  let accessToken = cookies.get('accessToken');
  let user = await jwt.decode(accessToken);



  let allDishes = await Dish.find();
  let maxDish = allDishes[0].likes.length;
  let allCategories = await Categorie.find();
  let maxCategorie = allCategories[0].likes.length;
  let bestDishName;
  let bestCategName;
  console.log(maxDish)
  await allDishes.forEach(elem => {
    if (elem.likes.length>=maxDish){
      maxDish = elem.likes.length;
      bestDishName = elem.nameDish;

    }

  });
  await allCategories.forEach(elem =>{
    if (elem.likes.length>=maxCategorie){
      maxCategorie = elem.likes.length;
      bestCategName = elem.name;
    }
  })
  // res.render('mainmenu',{
  //   bestDish: bestDishName, 
  //   bestCategorie: bestCategName, 
  //   user: user,
  //   categories: allCategories
  // });
  res.redirect('/mainmenu?bestDishName=' + bestDishName + '&bestCategName=' + bestCategName)
})



router.get('/addPage', async (req, res, next) => {
  let user = req.body.user;

  res.render('admin_addPage', {
    title: "Add categ",
    username: user.login,
    message: ''
  });
});

router.get('/dishes', async (req, res) => {
  let user = req.body.user;

  let url_parts = url.parse(req.url, true);
  let query = url_parts.query;
  let name = req.query.categorie;
  console.log('*')
  let Category = await Categorie.findOne({
    name: name
  });

  let Dishes = await Dish.find({
    categorie: Category._id
  })

  res.render('dishes', {
    categories: Category,
    dishes: Dishes,
    user: user
  });
});

router.get('/dishes/comments', async (req, res) => {
  let url_parts = url.parse(req.url, true);
  let query = url_parts.query;
  let dish_name = req.query.dish;

  let categorie_name = req.query.categorie;

  let cookies = await new Cookies(req, res);
  let token = cookies.get("accessToken");
  let user = await jwt.decode(token);
  console.log(user);

  let dish = await Dish.findOne({
    nameDish: dish_name
  });
  let comments = await Comments.find({
    dish_id: dish._id
  }).populate({
    path: 'user_id',
    select: 'login'
  });



  res.render('comments', {
    user: user,
    dish: dish_name,
    categorie: categorie_name,
    comments: comments
  });
});

router.post('/dishes/comments', async (req, res) => {
  let cookies = new Cookies(req, res);
  let token = cookies.get('refreshToken');
  let user = await jwt.decode(token);

  console.log(token);

  let url_parts = url.parse(req.url, true);
  let query = url_parts.query;
  let dish_name = req.query.dish;
  let categorie_name = req.query.categorie;

  let categorie = await Categorie.findOne({
    name: categorie_name
  });
  let dish = await Dish.findOne({
    nameDish: dish_name
  });

  let comment = {
    user_id: user.id,
    dish_id: dish._id,
    category_id: categorie._id,
    text: req.body.comments,
    time: new Date()
  }



  const newComment = new Comments(comment);
  await newComment.save();


  // res.render('comments', {
  //   user : user,
  //   dish: dish,
  //   categorie: categorie
  // });

  res.redirect('/mainmenu/dishes/comments?categorie=' + categorie_name + '&' + 'dish=' + dish_name);

});

router.post('/dishes/delete_comments', async (req, res) => {
  let url_parts = url.parse(req.url, true);
  let query = url_parts.query;
  let comment_id = req.query.comment_id;
  let categorie_name = req.query.categorie;
  let dish_name = req.query.dish
  console.log(comment_id);

  await Comments.findOneAndDelete({
    _id: comment_id
  });
  res.redirect('/mainmenu/dishes/comments?categorie=' + categorie_name + '&' + 'dish=' + dish_name);

})

module.exports = router;