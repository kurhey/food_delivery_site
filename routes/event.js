var express = require('express');
var router = express.Router();
const Cookies = require('cookies');
const jwt = require('jsonwebtoken');

const User = require('../models/user_model');
const {Dish} = require('../models/categorie_model');
const {Event} = require('../models/categorie_model');

router.post('/insert-event', async (req, res, next) => {
    let cookies = new Cookies(req, res);
    let refreshToken = cookies.get('refreshToken');
    let user = await jwt.decode(refreshToken);
    let dish = await Dish.findOne({nameDish: req.body.title});
    console.log(dish)
    let event = new Event({
        title: dish._id,
        user: user.id,
        start: req.body.start,
        end: req.body.end,
    });
    let excEvents = await Event.find({start:req.body.start});
    if (excEvents.length!=0)
    {
        excEvents.forEach((elem)=>{
            if (elem._id == user.id){
                elem.title.push(dish)
                          .save()
                          .then(result => {
                            res.render('calendar', {username: user.login});
                            })
                          .catch(err => {
                                console.log(err);
                                res.status(500).json({
                                    error: err
                                })
                            });


            }
        })
    }else{
      event.save()
           .then(result => {
            
           res.render('calendar', {username: user.login});
  
         })
           .catch(err => {
          console.log(err);
          res.status(500).json({
              error: err
          })
      });        
      }
    });

    router.get('/get-events', async (req, res, next) => {
    let allEvents = await Event.find();
    let eventsList = await allEvents.map(elem => {
        elem.title.forEach(async (item) => {
        let dish = await Dish.findOne({_id: item});
        item = dish.nameDish;
        })
        return elem;
        })
    res.send(eventsList);

});

router.post('/delete-event', async (req, res, next)=>{
    let user = await User.findOne({login: req.body.userName})
    await Event.findOneAndDelete({user: user._id, start: req.body.start, end: req.body.end});
    res.render('admin_addPage')
});

    module.exports = router;