const jwt = require('jsonwebtoken');
const Cookies = require('cookies');
const client = require('./../config/redis-config');
const secret = require('./../models/secret_model');

module.exports.tokenVerification = async (req, res, next) => {
    let cookies = new Cookies(req, res);

    let accessToken = cookies.get('accessToken');
    let refreshToken = cookies.get('refreshToken');

    let user = await jwt.decode(refreshToken);

    console.log(user.id);
    await jwt.verify(accessToken, secret.accesSecret, async (AccessTokenError, result) => {
        if(AccessTokenError) {
            console.log('Access Token Error ' + AccessTokenError);
            await client.get(user.id.toString(), async (redisError, redisRefreshToken) => {

                if (redisError) {
                    console.log(redisError);
                    res.redirect('/');
                }
                else {
                    if(refreshToken != redisRefreshToken) {
                        console.log('Refresh != Redis Refresh ');
                        res.clearCookie('accessToken');
                        res.clearCookie('refreshToken');
                        res.redirect('/');
                    }
                    else {
                        await jwt.verify(refreshToken, secret.refreshSecret, async (RefreshTokenError, result) => {
                            if ( RefreshTokenError) {
                                console.log('Refresh Token Error ' + RefreshTokenError);
                                res.clearCookie('accessToken');
                                res.clearCookie('refreshToken');
                                res.redirect('/');
                            } else {

                                let newAccessToken = await jwt.sign({login : user.login, role: user.role}, secret.accesSecret, {expiresIn : secret.accessLife});
                                let newRefreshToken = await jwt.sign({login : user.login, id : user.id}, secret.refreshSecret, {expiresIn : secret.refreshLife});
                                
                                cookies.set('accessToken', newAccessToken)
                                cookies.set('refreshToken', newRefreshToken)
                                client.set(user.id.toString(), newRefreshToken);
                                console.log('*');
                                user = await jwt.decode(newAccessToken);
                                req.body.user = user;

                                next();
                            }
                        });

                    }
                }
            });
        }
        else {
            
            user = await jwt.decode(accessToken);
            req.body.user = user;
            next();
        }
    });
}
   
    
