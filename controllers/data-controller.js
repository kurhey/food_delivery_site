var express = require('express');
var mongoose = require('mongoose');
const Cookies = require('cookies');
const jwt = require('jsonwebtoken');
const {Categorie} = require('../models/categorie_model');
const url = require('url');

module.exports.getData = async (req, res, next) => {
  let cookies = new Cookies(req, res);
  let accessToken = cookies.get('accessToken');
  let user = await jwt.decode(accessToken);

  let url_parts = url.parse(req.url, true);
  let query = url_parts.query;
  let bestDishName = req.query.bestDishName;
  let bestCategName = req.query.bestCategName ;

  let allCategories = await Categorie.find();

  res.render('mainmenu', {
    categories: allCategories, 
    user: user,
    bestDish: bestDishName, 
    bestCategorie: bestCategName 
  });


};
