const multiparty = require('multiparty');
const mongoose = require('mongoose');
const nodemailer = require('nodemailer');
const jwt = require('jsonwebtoken');
const secret = require('./../models/secret_model');
const localStorage = require('localStorage');
const bcrypt = require('bcryptjs');
const User = require('./../models/user_model');
const Cookies = require('cookies');


module.exports.registr = async (req, res, next) => {
    
    let form = new multiparty.Form();


    form.parse(req, async (err, fields, files) => {
        if(fields.login[0] != '' && fields.password[0] != '' && fields.confirm_password[0] != ''){  
            const checkSameUser = await User.findOne({'login' : fields.login[0]});
            if (checkSameUser) {
                res.render('index', {info : "This email is used"});
                console.log('email exist');
                return;
            } 

            if (fields.password[0] != fields.confirm_password[0]) {
                res.render('register', {info : "Password does not match"});
                return;
            }
            
            
            const hash = await bcrypt.hash(fields.password[0], 10);
            let user = {
                login : fields.login[0],
                password : hash,
                role : 1,
                active : false
            }  

            const newUser = await new User(user);
            await newUser.save();
            user = await User.findOne({'login' : fields.login[0]})
            
            console.log('user saved');
            console.log('* ' + user);
            try{

                let receiver = user.login;

                let token = await jwt.sign({login : user.login, id : user._id}, secret.accesSecret,  {expiresIn : secret.accessLife});
                console.log(token);

                const transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                      type: 'OAuth2',       
                      user: 'vetrov098@gmail.com',
                      clientId: '627559671072-rflp6nsfhshpgth79oreg5kn6qk7noec.apps.googleusercontent.com',
                      clientSecret: 'FsUB7hctfLRkivHvdt6bo8ax',
                      refreshToken: '1/JhYWtScGRC54pOaVNh_dcEtikgkQkLJuPNHsujx6TeM',
                      //accessToken: 'ya29.GltVBkDZGAshM_EudbqhJtUaRLMIc2DXe5t6Ew4rbtAf57v7PXXjMNIu1migtfZ8upmcEtloG_ct8aToTSwShoZXPuJdCI-QuPJgjxsCscfzS-53gzARuOY26QgJ',
                      expires: 3599
                    }
                });
                
                const mailOptions = {
                    from: 'Евгений Ветров <vetrov098@gmail.com>' , // sender address
                    to: receiver, // list of receivers
                    subject: 'Verification', // Subject line
                    text: 'Test', // plain text body
                    html: '<h1>For confirmation  <a href="http://localhost:3000/validation?token=' + token + '">click here</a></h1>'
                };
                       
                await transporter.sendMail(mailOptions, (err, res) => {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log('mail send');
                    }
                      
                });
                res.render('index', {info: "Succes registration"});

            } catch(error){
                console.log(error);
                res.render('index', {info: "service unavailable"});
            }
        } else res.render('index', {info : "Input your credentials, please"});
    });
}
