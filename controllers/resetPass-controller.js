const mongoose = require('mongoose');
const userModel = require('./../models/user_model');
const url = require('url');
const secret = require('./../models/secret_model');
const jwt = require('jsonwebtoken');
const User = require('./../models/user_model');
const nodemailer = require('nodemailer');
const Cookies = require('cookies');
const multiparty = require('multiparty');
const bcrypt = require('bcryptjs');

module.exports.passReset = async (req, res, next) => {
    let form = new multiparty.Form();

    form.parse(req, async (err, fields, files) => {

        if(fields.login[0] != "" && fields.password[0] != "" && fields.confirm_password[0] != "") {
            let user = await User.findOne({'login' : fields.login[0]});

            if(user){
                if (fields.password[0] == fields.confirm_password[0]) {
                    let pass = fields.password[0];
                    let hash = await bcrypt.hash(pass, 10);

                    await User.findOneAndUpdate({'login' : fields.login[0]}, {'password' : hash}, {upsert:true}, async (error, doc) => {
                        if (error) {
                            console.log(error);
                        } else {
                            const transporter = nodemailer.createTransport({
                                service: 'gmail',
                                auth: {
                                    type: 'OAuth2',       
                                    user: 'vetrov098@gmail.com',
                                    clientId: '627559671072-rflp6nsfhshpgth79oreg5kn6qk7noec.apps.googleusercontent.com',
                                    clientSecret: 'FsUB7hctfLRkivHvdt6bo8ax',
                                    refreshToken: '1/JhYWtScGRC54pOaVNh_dcEtikgkQkLJuPNHsujx6TeM',
                                    //accessToken: 'ya29.GltVBkDZGAshM_EudbqhJtUaRLMIc2DXe5t6Ew4rbtAf57v7PXXjMNIu1migtfZ8upmcEtloG_ct8aToTSwShoZXPuJdCI-QuPJgjxsCscfzS-53gzARuOY26QgJ',
                                    expires: 3599
                                }
                            });
                            
                            const mailOptions = {
                                from: 'Евгений Ветров <vetrov098@gmail.com>' , // sender address
                                to: fields.login[0], // list of receivers
                                subject: 'Reset password', // Subject line
                                text: 'Test', // plain text body
                                html: '<h1>Your new password: '+ pass + '</h1>'
                            };

                            
                            try {
                                await transporter.sendMail(mailOptions);
                            } catch (err) {
                                console.log(err);
                                res.render('index', {info: "Error"});
                                return;
                            }

                            res.render('index', {title: 'Log In', info: 'Email with new pass was sent'});
                        }
                        
                    });
                } else res.render('reset_pass', {info : "Passwords does not match"});

            } else res.render('reset_pass', {info : "Wrong login. Please, register"});
        

        } else res.render('reset_pass', {info : "Fill all fields"});
    });
}