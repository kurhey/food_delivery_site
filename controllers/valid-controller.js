const mongoose = require('mongoose');
const userModel = require('./../models/user_model');
const url = require('url');
const secret = require('./../models/secret_model');
const jwt = require('jsonwebtoken');
const User = require('./../models/user_model');
const nodemailer = require('nodemailer');
const Cookies = require('cookies');
const client = require('./../config/redis-config');

module.exports.valid = async (req, res) => {

    let url_parts = url.parse(req.url, true);
    let query = url_parts.query;
    let token = req.query.token;
    let user = await jwt.decode(token);
    console.log(user);

    try {
        await jwt.verify(token, secret.accesSecret, async (err, result) => {
            if(err){

                console.log(err);

                let newToken = await jwt.sign({login : user.login}, secret.accesSecret, {expiresIn : secret.accessLife});
                
                let transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                      type: 'OAuth2',       
                      user: 'vetrov098@gmail.com',
                      clientId: '627559671072-rflp6nsfhshpgth79oreg5kn6qk7noec.apps.googleusercontent.com',
                      clientSecret: 'FsUB7hctfLRkivHvdt6bo8ax',
                      refreshToken: '1/JhYWtScGRC54pOaVNh_dcEtikgkQkLJuPNHsujx6TeM',
                      //accessToken: 'ya29.GltVBkDZGAshM_EudbqhJtUaRLMIc2DXe5t6Ew4rbtAf57v7PXXjMNIu1migtfZ8upmcEtloG_ct8aToTSwShoZXPuJdCI-QuPJgjxsCscfzS-53gzARuOY26QgJ',
                      expires: 3599
                    }
                });
                
                let mailOptions = {
                    from: 'Евгений Ветров <vetrov098@gmail.com>' , // sender address
                    to: user.login, // list of receivers
                    subject: 'Verification', // Subject line
                    text: 'Test', // plain text body
                    html: '<h1>New link for confirmation  <a href="http://localhost:3000/validation?token=' + newToken + '">click here</a></h1>'
                };
                       
                transporter.sendMail(mailOptions, (err, result) => {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log('New mail sent');
                        res.send('<h1>New mail sent. Confirm, please</h1>');
                    }
                });
    
            }
            else {
    
                let query = {'login' : user.login};
                let update = {active : true};
                
                await User.findOneAndUpdate(query, update, {upsert:true}, async (err, doc) => {
                    if (err) {
                        console.log(err);
                        res.render('index', {info : "Something is wrong"});
                    }
                    else {
                        console.log('Role true');

                        let accessToken = await jwt.sign({login : user.login, role: user.role}, secret.accesSecret, {expiresIn : secret.accessLife});
                        let refreshToken = await jwt.sign({login : user.login, id : user._id}, secret.refreshSecret, {expiresIn : secret.refreshLife});

                        var cookies = new Cookies(req, res)

                        cookies.set('accessToken', accessToken);
                        cookies.set('refreshToken', refreshToken);

                        console.log(user);
                        client.set(user.id.toString(), refreshToken);

                        res.render('index', {info: "Confirmed. Log in, please"});
                    }
                });
            }
        });
    }catch (error){
        console.log(error);
    }

}
