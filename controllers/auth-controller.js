const multiparty = require('multiparty');
const mongoose = require('mongoose');
const userModel = require('./../models/user_model');
const secret = require('./../models/secret_model');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const User = require('./../models/user_model');
const Cookies = require('cookies');
let client = require('./../config/redis-config');



module.exports.auth = async (req, res, next) => {

    let form = new multiparty.Form();


    form.parse(req, async (err, fields, files) => {
        if(fields)
        {
            if(fields.login[0] != '' && fields.password[0] != ''){

                const user = await User.findOne({'login' : fields.login[0]});

                if(user) {

                    await bcrypt.compare(fields.password[0], user.password, async (err, result) => {
                        console.log('pass ' + result);
                        if (result) {
                            console.log('active ' + user.active);
                            if(user.active) {
                                
                                

                                let accessToken = await jwt.sign({login : user.login, role: user.role}, secret.accesSecret, {expiresIn : secret.accessLife});
                                let refreshToken = await jwt.sign({login : user.login, id : user._id}, secret.refreshSecret, {expiresIn : secret.refreshLife});

                                var cookies = new Cookies(req, res)
                                console.log('user id: '+user._id);
                                client.set(user._id.toString(), refreshToken);
                                //client.quit();

                                cookies.set('accessToken', accessToken)
                                cookies.set('refreshToken', refreshToken)

                                
                                res.redirect('/mainmenu');

                            } else {

                                res.render('index', {info : "Please, confirm your account"});

                            }

                        } else {

                            res.render('index', {info : "Password is wrong"});

                        }
                    })

                } else {
                    res.render('index', {info : "Please, register"});
                }
                
            }
        }

    });
}

